import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  form: FormGroup = this.fb.group({
    size: [2, [Validators.required, Validators.min(2), Validators.max(20)]],
    min: [1, [Validators.required, Validators.min(1), Validators.max(100)]],
    max: [100, [Validators.required, Validators.min(1), Validators.max(100)]],
    moveTo: ['start', Validators.required],
  })

  array = [];

  constructor( private fb:FormBuilder) { }

  ngOnInit(): void {

  }

  random(): number {
    return Math.floor(Math.random() * (this.form.value.max - this.form.value.min + 1) + this.form.value.min);
  }

  generation():void {
    this.array = [];
    for(let i=1; i <= this.form.value.size; i++) {
      this.array.push(this.random());
    }
  }

  sort(index, item) {
    this.array.splice(index, 1)
    if(this.form.value.moveTo === 'end') {
      this.array.push(item);
    } else {
      this.array.unshift(item);
    }
  }
}
